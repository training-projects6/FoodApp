Food app demo  project.

- Main technologies  and patterns: MVVM, Combine.

- Goal was to make all UI from scratch according to Figma prototype, 
download data from API and implement it in UI. Add teg filtering to dishes view. Create logic for basket wich can calculate summ of order, delete dishes if amout < 1.

<img src="/uploads/0a40cfedfb1721f0076fafb4ba2107b9/Simulator.gif" width="320" >

<img src="/uploads/0a1fed4ed00cb59292bc9ec5afbb9108/Simulator_Screen_Shot_-_iPhone_14_Pro_Max_-_2023-07-07_at_12.09.51.png" width="320" >

<img src="/uploads/870be57aee56abd3d9eb6e43944a496e/Simulator_Screen_Shot_-_iPhone_14_Pro_Max_-_2023-07-07_at_12.10.01.png" width="320" >

<img src="/uploads/6be90a551c6544848aea165724de901a/Simulator_Screen_Shot_-_iPhone_14_Pro_Max_-_2023-07-07_at_12.10.07.png" width="320" >

<img src="/uploads/85eb469137012cc2354f3e9a3d7b4456/Simulator_Screen_Shot_-_iPhone_14_Pro_Max_-_2023-07-11_at_11.05.42.png" width="320" >

<img src="/uploads/74262b1a93b52130ff883163dbcd846e/Simulator_Screen_Shot_-_iPhone_14_Pro_Max_-_2023-07-07_at_12.10.29.png" width="320" >

<img src="/uploads/e328d71624ebf12a424ec07885fb47c4/Simulator_Screen_Shot_-_iPhone_14_Pro_Max_-_2023-07-07_at_12.10.36.png" width="320" >
