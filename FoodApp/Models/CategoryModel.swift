//
//  CategoryModel.swift
//  FoodApp
//
//  Created by Artem Soloviev on 01.07.2023.
//

import Foundation

struct Welcome: Codable {
    var сategories: [Сategory]
}

struct Сategory: Codable, Identifiable {
    var id: Int
    var name: String
    var imageURL: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case imageURL = "image_url"
    }
}
