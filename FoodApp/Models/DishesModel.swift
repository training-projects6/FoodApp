//
//  DishesModel.swift
//  FoodApp
//
//  Created by Artem Soloviev on 01.07.2023.
//

import Foundation

struct WelcomeDish: Codable {
    var dishes: [Dish]
}

struct Dish: Codable, Identifiable, Equatable, Hashable{
    var id: Int
    var name: String
    var price: Int
    var weight: Int
    var description: String
    var imageURL: String
    var tegs: [Teg]
    
    enum CodingKeys: String, CodingKey {
        case id, name, price, weight, description
        case imageURL = "image_url"
        case tegs
    }
}

enum Teg: String, Codable, Hashable, CaseIterable, Identifiable {
    case всеМеню = "Все меню"
    case сРисом = "С рисом"
    case сРыбой = "С рыбой"
    case салаты = "Салаты"
    
    var id: String {
        self.rawValue
    }
}
