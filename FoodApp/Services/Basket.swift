//
//  Basket.swift
//  FoodApp
//
//  Created by Artem Soloviev on 03.07.2023.
//

import Foundation

class Basket: ObservableObject {

    @Published var addedDish:[Dish] = [] {
        didSet {
            duplicationDish = addedDish.removingDuplicates()
        }
    }
    @Published var  duplicationDish:[Dish] = []
    @Published var price:[Int] = []{
        didSet {
            total = price.reduce(0,+)
        }
    }
    @Published var total = 0
    @Published var showingPopover = false
    @Published var selected:Dish?
    
}
