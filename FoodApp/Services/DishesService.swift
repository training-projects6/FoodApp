//
//  DishesService.swift
//  FoodApp
//
//  Created by Artem Soloviev on 02.07.2023.
//

import Foundation
import Combine

struct DishesService {
    
    static func getCategory() -> AnyPublisher<WelcomeDish, Error> {
        let url = URL(string: "https://run.mocky.io/v3/aba7ecaa-0a70-453b-b62d-0e326c859b3b")!
        
        return APIService
            .get(for: url)
    }
}
