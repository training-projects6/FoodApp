//
//  CategoryService.swift
//  FoodApp
//
//  Created by Artem Soloviev on 01.07.2023.
//

import Foundation
import Combine

struct CategoryService {
    
    static func getCategory() -> AnyPublisher<Welcome, Error> {
        let url = URL(string: "https://run.mocky.io/v3/058729bd-1402-4578-88de-265481fd7d54")!
        
        return APIService
            .get(for: url)
    }
}
