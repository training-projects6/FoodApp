//
//  FoodAppApp.swift
//  FoodApp
//
//  Created by Artem Soloviev on 01.07.2023.
//

import SwiftUI

@main
struct FoodAppApp: App {
    var body: some Scene {

        WindowGroup {
            ContentView()
        }
    }
}
