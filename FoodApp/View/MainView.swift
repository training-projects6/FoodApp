//
//  MainView.swift
//  FoodApp
//
//  Created by Artem Soloviev on 01.07.2023.
//

import SwiftUI

struct MainView: View {
    @ObservedObject var categorys:CategoryViewModel
    @ObservedObject var dishes: DishesViewModel
    @ObservedObject var basket: Basket
    
    var body: some View {
        ScrollView (.vertical) {
            ForEach(categorys.welcome.сategories) { category in
                NavigationLink(destination: CategoryView(categorys: category, dishes: dishes, basket: basket)) {
                    ZStack(alignment: .topLeading) {
                        AsyncImage(url: URL (string: category.imageURL)) { image in
                            image
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .scaledToFill()
                                .frame(maxWidth: .infinity,  maxHeight: .infinity, alignment: .center)
                                .clipShape(RoundedRectangle(cornerRadius: 10))
                        } placeholder: {
                            ProgressView()
                        }
                        Text(category.name)
                            .padding()
                            .font(.custom("SF Pro Display", size: 20))
                            .fontWeight(.medium)
                    }
                }.buttonStyle(.plain)
            }
        }.padding()
            .scrollIndicators(.hidden)
    }
}

//struct MainView_Previews: PreviewProvider {
//    static var previews: some View {
//        MainView(categorys:CategoryViewModel(), dishes: DishesViewModel(),basket: Basket())
//    }
//}
