//
//  BasketView.swift
//  FoodApp
//
//  Created by Artem Soloviev on 01.07.2023.
//

import SwiftUI

struct BasketView: View {
    @ObservedObject var basket: Basket
    
    var body: some View {
        VStack{
            ScrollView(.vertical){
                ForEach(basket.duplicationDish){ element in
                    HStack{
                        HStack(alignment: .center) {
                            AsyncImage(url: URL (string: element.imageURL)) { image in
                                image
                                    .resizable()
                                    .aspectRatio(1, contentMode: .fit)
                                    .padding()
                                    .frame(width: 60, height: 60)
                                    .background(Color("Vgrid"))
                                    .clipShape(RoundedRectangle(cornerRadius: 10))
                            } placeholder: {
                                ProgressView()
                            }
                            VStack(alignment: .leading){
                                Text(element.name)
                                    .font(.custom("SF Pro Display", size: 14))
                                HStack {
                                    Text(element.price, format: .currency(code: "RUB").precision(.fractionLength(0)))
                                        .font(.custom("SF Pro Display", size: 14))
                                    Text("\u{2022} \(element.weight) г")
                                        .font(.custom("SF Pro Display", size: 14))
                                        .foregroundColor(.black.opacity(0.65))
                                }
                            }
                        }
                        Spacer()
                        HStack{
                            Button{
                                if let index = basket.addedDish.firstIndex(of: element) {
                                    basket.addedDish.remove(at: index)
                                    basket.price.remove(at: index)
                                }
                            }label: {
                                Image(systemName: "minus")
                            }
                            let b =  basket.addedDish.filter({ dish in
                                dish.id == element.id})
                            Text("\(b.count)")
                                .padding(.horizontal)
                            Button{
                                basket.addedDish.append(element)
                                basket.price.append(element.price)
                            }label: {
                                Image(systemName: "plus")
                            }
                        }.padding(.horizontal)
                            .padding(.vertical, 10)
                            .frame(width: 140)
                            .foregroundColor(.black)
                            .fontWeight(.medium)
                            .background(Color("Button"))
                            .clipShape(RoundedRectangle(cornerRadius: 10 ,style: .continuous))
                    }
                }.padding()
            }
            .scrollIndicators(.hidden)
            if !basket.duplicationDish.isEmpty {
                Button{
                    
                }label: {
                    Text("Оплатить \(basket.total, format: .currency(code: "RUB").precision(.fractionLength(0)))")
                        .font(.custom("SF Pro Display", size: 16))
                        .fontWeight(.medium)
                        .foregroundColor(.white)
                        .padding()
                        .frame(maxWidth: .infinity)
                        .background(.blue)
                        .clipShape(RoundedRectangle(cornerRadius: 10 ,style: .continuous))
                }.padding()
            }
        }
    }
}

struct BasketView_Previews: PreviewProvider {
    static var previews: some View {
        BasketView(basket: Basket())
    }
}
