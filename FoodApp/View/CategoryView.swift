//
//  CategoryView.swift
//  FoodApp
//
//  Created by Artem Soloviev on 01.07.2023.
//

import SwiftUI
import PopupView

struct CategoryView: View {
    @Environment(\.dismiss) private var dismiss
    var categorys: Сategory
    @ObservedObject var dishes: DishesViewModel
    @ObservedObject var basket: Basket
    let columns = [
        GridItem(.adaptive(minimum: 80)),
        GridItem(.adaptive(minimum: 80)),
        GridItem(.adaptive(minimum: 80)),
    ]
    @State  var selectedTegs:Teg = .всеМеню
    
    var body: some View {
        VStack{
            ScrollView(.horizontal){
                HStack{
                    ForEach(Teg.allCases) {teg in
                        Button {
                            withAnimation(.default){
                                selectedTegs = teg
                                dishes.filterDishes(collection: [selectedTegs])
                            }
                        } label: {
                            Text(teg.rawValue)
                                .padding(.vertical, 10)
                                .padding(.horizontal, 25)
                                .font(.custom("SF Pro Display", size: 14))
                                .background(selectedTegs == teg ? Color.blue : Color("Vgrid"))
                                .foregroundColor(selectedTegs == teg ? .white : .black)
                                .clipShape(RoundedRectangle(cornerRadius: 10,style: .continuous))
                        }
                    }
                }.padding([.top,.horizontal])
            } .scrollIndicators(.hidden)
            Spacer()
            ScrollView {
                LazyVGrid(columns: columns) {
                    ForEach(dishes.filteredDishes){ dish in
                        VStack(alignment: .leading){
                            Button {
                                basket.selected = dish
                                basket.showingPopover = true
                            }label: {
                                AsyncImage(url: URL (string: dish.imageURL)) { image in
                                    image
                                        .resizable()
                                        .aspectRatio(1, contentMode: .fit)
                                        .clipped()
                                        .padding()
                                        .background(Color("Vgrid"))
                                        .clipShape(RoundedRectangle(cornerRadius: 10))
                                } placeholder: {
                                    ProgressView()
                                }
                            }
                            Text(dish.name)
                                .font(.custom("SF Pro Display", size: 14))
                                .padding(.bottom)
                            Spacer()
                        }
                    }
                }
                .padding()
            }.padding(.bottom, 0.1)
                .scrollIndicators(.hidden)
        }
        .toolbar{
            ToolbarItem(placement: .navigationBarTrailing) {
                Image("Avatar")
                    .resizable()
                    .scaledToFill()
                    .frame(width: 44, height: 44)
                    .clipShape(Circle())
            }
            ToolbarItem(placement: .navigationBarLeading) {
                Button {
                    dismiss()
                }label: {
                    Image(systemName: "chevron.backward")
                        .foregroundColor(.black)
                }
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationTitle(categorys.name)
        .popup(isPresented: $basket.showingPopover) {
            DishDetailView(dish: basket.selected!, basket: basket)
                .padding()
        } customize: {
            $0
                .isOpaque(true)
                .backgroundColor(.black.opacity(0.5))
        }
    }
}

//struct CategoryView_Previews: PreviewProvider {
//    static var previews: some View {
//        CategoryView(categorys: Category)
//    }
//}
