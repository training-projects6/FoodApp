//
//  DishDetailView.swift
//  FoodApp
//
//  Created by Artem Soloviev on 03.07.2023.
//

import SwiftUI

struct DishDetailView: View {
    @Environment(\.dismiss) private var dismiss
    var dish: Dish
    @ObservedObject var basket: Basket
    
    var body: some View {
        VStack(alignment: .leading) {
            ZStack(alignment: .topTrailing){
                AsyncImage(url: URL (string: dish.imageURL)) { image in
                    image
                        .resizable()
                        .aspectRatio(1, contentMode: .fit)
                        .padding()
                        .background(Color("Vgrid"))
                        .clipShape(RoundedRectangle(cornerRadius: 10))
                } placeholder: {
                    ProgressView()
                }
                HStack{
                    Button {
                        
                    }label: {
                        Image(systemName: "heart")
                            .padding()
                            .font(.title2)
                            .background(.white)
                            .clipShape(RoundedRectangle(cornerRadius: 10))
                    }.padding(.trailing, 5)
                    Button{
                        basket.showingPopover = false
                    }label: {
                        Image(systemName: "multiply")
                            .padding()
                            .font(.title)
                            .background(.white)
                            .clipShape(RoundedRectangle(cornerRadius: 10))
                    }
                } .padding()
                    .foregroundColor(.black)
            }
            VStack(alignment: .leading){
                Text(dish.name)
                    .font(.custom("SF Pro Display", size: 16))
                    .fontWeight(.medium)
                HStack {
                    Text(dish.price, format: .currency(code: "RUB").precision(.fractionLength(0)))
                        .font(.custom("SF Pro Display", size: 14))
                    Text("\u{2022} \(dish.weight) г")
                        .font(.custom("SF Pro Display", size: 14))
                        .foregroundColor(.black.opacity(0.65))
                }.padding(.vertical, 3)
                Text(dish.description)
                    .font(.custom("SF Pro Display", size: 14))
                    .foregroundColor(.black.opacity(0.5))
                Button{
                    if !basket.addedDish.contains(where: { $0.id == dish.id }) {
                        basket.addedDish.append(dish)
                        basket.price.append(dish.price)
                        basket.showingPopover = false
                    }
                }label: {
                    Text("Добавить в корзину")
                        .font(.custom("SF Pro Display", size: 16))
                        .fontWeight(.medium)
                        .foregroundColor(.white)
                        .padding()
                        .frame(maxWidth: .infinity)
                        .background(.blue)
                        .clipShape(RoundedRectangle(cornerRadius: 10))
                }
            }
        }.padding()
            .background(.white)
            .clipShape(RoundedRectangle(cornerRadius: 10))
            .navigationBarBackButtonHidden(true)
            .navigationTitle(dish.name)
    }
}

//struct DishDetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        DishDetailView(dish: Dish(id: 1, name: "", price: 1, weight: 1, description: "", imageURL: "", tegs: [Teg.всеМеню]), basket: Basket(), selectedItem: selectedItem)
//    }
//}
