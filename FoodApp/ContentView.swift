//
//  ContentView.swift
//  FoodApp
//
//  Created by Artem Soloviev on 01.07.2023.
//

import SwiftUI

struct ContentView: View {
    @StateObject var mainView = CategoryViewModel()
    @StateObject var dishes = DishesViewModel()
    @StateObject var basket =  Basket()
    let date:Date
    let dateFormatter: DateFormatter
    init() {
        date = Date.now
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM, YYYY"
        
    }
    @State private var selection = 0
    
    var mainTab: some View {
        MainView(categorys: mainView, dishes: dishes,basket: basket)
    }
    
    var searchTab: some View {
        SearchView()
    }
    
    var basketTab: some View {
        BasketView(basket: basket)
    }
    var accountTab: some View {
        AccountView()
    }
    var body: some View {
        TabView(selection: $selection){
            NavigationStack{
                mainTab
                    .toolbar{
                        ToolbarItem(placement: .navigationBarLeading) {
                            HStack(){
                                Image("MapPin")
                                VStack(alignment: .leading){
                                    Text("Санкт-Петербург")
                                        .font(.custom("SF Pro Display", size: 18))
                                        .fontWeight(.medium)
                                    Text(date, formatter: dateFormatter)
                                        .font(.custom("SF Pro Display", size: 14))
                                        .foregroundColor(.black.opacity(0.5))
                                }
                            }
                        }
                        ToolbarItem(placement: .navigationBarTrailing){
                            Image("Avatar")
                                .resizable()
                                .scaledToFill()
                                .frame(width: 44, height: 44)
                        }
                    }
            }
            .tabItem {
                Image("Main").renderingMode(.template)
                Text("Главная")
            }
            
            .tag(0)
            searchTab.tabItem {
                Image("Search").renderingMode(.template)
                Text("Поиск")
            }
            .tag(1)
            NavigationStack{
                basketTab
                    .toolbar{
                        ToolbarItem(placement: .navigationBarLeading) {
                            HStack(){
                                Image("MapPin")
                                VStack(alignment: .leading){
                                    Text("Санкт-Петербург")
                                        .font(.custom("SF Pro Display", size: 18))
                                        .fontWeight(.medium)
                                    Text(date, formatter: dateFormatter)
                                        .font(.custom("SF Pro Display", size: 14))
                                        .foregroundColor(.black.opacity(0.5))
                                }
                            }
                        }
                        ToolbarItem(placement: .navigationBarTrailing){
                            Image("Avatar")
                                .resizable()
                                .scaledToFill()
                                .frame(width: 44, height: 44)
                        }
                    }
            }
            .tabItem {
                Image("Basket").renderingMode(.template)
                Text("Корзина")
            }
            .tag(2)
            accountTab.tabItem {
                Image("Account").renderingMode(.template)
                Text("Аккаунт")
            }
            .tag(2)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
