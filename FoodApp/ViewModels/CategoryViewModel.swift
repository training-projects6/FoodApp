//
//  CategoryViewModel.swift
//  FoodApp
//
//  Created by Artem Soloviev on 01.07.2023.
//

import Foundation
import Combine

final class CategoryViewModel: ObservableObject {
    
    @Published var welcome: Welcome = Welcome(сategories: [])
    var cancellables = Set<AnyCancellable>()
    
    init() {
        load()
    }
    
    func load() {
        CategoryService
            .getCategory()
            .receive(on: RunLoop.main)
            .sink { (completion) in
                switch completion {
                case .failure(let error):
                    print(error)
                    return
                case .finished:
                    return
                }
            } receiveValue: {[weak self](returnedWelcome) in
                self?.welcome = returnedWelcome
            }
            .store(in: &cancellables)
    }
}
