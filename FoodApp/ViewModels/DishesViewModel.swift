//
//  DishesViewModel.swift
//  FoodApp
//
//  Created by Artem Soloviev on 01.07.2023.
//

import Foundation
import Combine

final class DishesViewModel: ObservableObject {
    
    @Published var welcomeDish: WelcomeDish = WelcomeDish(dishes: [])
    @Published var filteredDishes:[Dish] = []
    var cancellables = Set<AnyCancellable>()
    
    init() {
        load()
    }
    
    func load() {
        DishesService
            .getCategory()
            .receive(on: RunLoop.main)
            .sink { (completion) in
                switch completion {
                case .failure(let error):
                    print(error)
                    return
                case .finished:
                    return
                }
            } receiveValue: {[weak self](returnedWelcomeDish) in
                self?.welcomeDish = returnedWelcomeDish
                self?.filteredDishes = returnedWelcomeDish.dishes
            }
            .store(in: &cancellables)
    }
    func filterDishes(collection:[Teg])  {
        filteredDishes = welcomeDish.dishes.filter({ dish in
            dish.tegs.contains(collection)
        })
    }
}
